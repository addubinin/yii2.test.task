<?php

use yii\db\Migration;

/**
 * Class m181226_190546_create_subscription
 */
class m181226_190546_create_subscription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = \Yii::$app->getDb();

        $subTable = $db->getTableSchema('subscription');
        if (!$subTable) {
            $this->createTable('subscription', [
                'id' => $this->primaryKey(),
                'last_date' => $this->integer(),
                'user_id' => $this->integer()
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = \Yii::$app->getDb();

        $subTable = $db->getTableSchema('subscription');
        if ($subTable) {
            $this->dropTable('subscription');
        }
    }
}
