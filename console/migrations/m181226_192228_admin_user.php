<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m181226_192228_admin_user
 */
class m181226_192228_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $admin = User::find()->where(['username' => 'admin'])->one();
        if (!$admin) {
            $admin = new User();
            $admin->username = 'admin';
            $admin->auth_key = \Yii::$app->security->generateRandomString();
            $admin->password_hash = \Yii::$app->getSecurity()->generatePasswordHash('admin');
            $admin->email = 'admin@gmail.com';
            $admin->created_at = time();
            $admin->updated_at = time();
            $admin->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $admin = User::find()->where(['username' => 'admin'])->one();
        if ($admin) {
            User::deleteAll(['username' => 'admin']);
        }
    }
}
