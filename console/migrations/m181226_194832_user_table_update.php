<?php

use yii\db\Migration;

/**
 * Class m181226_194832_user_table_update
 */
class m181226_194832_user_table_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'first_name', $this->string());
        $this->addColumn('user', 'second_name', $this->string());
        $this->addColumn('user', 'initial', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'second_name');
        $this->dropColumn('user', 'initial');
    }
}
