<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.12.2018
 * Time: 23:31
 */

namespace common\models;

use Yii;
use yii\base\Model;


class UserForm extends Model {

    public $username;
    public $password;
    public $email;
    public $firstName;
    public $secondName;
    public $initial;
    public $subLastDate;

    private $user = null;

    public function rules()
    {
        return [
            [['username', 'email'], 'required', 'message' => '{attribute} не может быть пустым'],
            [
                'username', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'username', 'message' => '{attribute} должен быть уникальным',
                'when' => function ($model) {
                    return $this->user && $this->user->username !== $model->username;
                }
            ],
            [
                'email', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email', 'message' => '{attribute} {value} уже используется',
                'when' => function ($model) {
                    return $this->user && $this->user->email !== $model->email;
                }
            ],
            ['password', 'required', 'when' => function ($model) {
                return is_null($this->user);
            }, 'enableClientValidation' => false],
            ['email', 'email', 'message' => '{attribute} не правильный формат'],
            ['username', 'match', 'pattern' => '/[a-zA-Z0-9]/i', 'message' => '{attribute} должен содержать только цифры и латинские буквы'],
            [['username', 'email', 'firstName', 'secondName', 'initial', 'subLastDate'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Электронная почта',
            'password' => 'Пароль'
        ];
    }

    public function fillUser(User $user)
    {
        if (!empty($this->password)) {
            $user->setPassword($this->password);
        }

        if (!$this->user) {
            $user->generateAuthKey();
        }

        $user->username = $this->username;
        $user->first_name = $this->firstName;
        $user->second_name = $this->secondName;
        $user->initial = $this->initial;
        $user->email = $this->email;
    }

    private function checkSubscription($user)
    {
        $subscription = Subscription::findOne(['user_id' => $user->id]);
        if (!empty($this->subLastDate)) {
            if (!$subscription) {
                $subscription = new Subscription();
            }

            $subscription->last_date = $this->subLastDate;
            $subscription->user_id = $user->id;
            $subscription->save();
        } else {
            if ($subscription) {
                $subscription->delete();
            }
        }
    }

    public function makeUser()
    {
        $newUser = new User();
        $this->fillUser($newUser);
        $this->checkSubscription($newUser);
        $newUser->save();

        return $newUser;
    }

    public function updateUser($id)
    {
        $user = User::findOne(['id' => $id]);
        $this->fillUser($user);
        $this->checkSubscription($user);
        $user->save();
        return $user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        $this->username = $user->username;
        $this->firstName = $user->first_name;
        $this->secondName = $user->second_name;
        $this->initial = $user->initial;
        $this->email = $user->email;

        $subscription = $user->getSubscription();
        $this->subLastDate = $subscription->last_date;
    }
}