<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 27.12.2018
 * Time: 7:57
 */

namespace common\models;


use yii\db\ActiveRecord;

class Subscription extends ActiveRecord {


    public static function tableName()
    {
        return 'subscription';
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->last_date = \DateTime::createFromFormat('d.m.Y H:i:s', $this->last_date . ' 23:59:59')->getTimestamp();

        return true;
    }

    public function init()
    {
        parent::init();
        $this->last_date = date('d.m.Y H:i:s', $this->last_date);
    }

}