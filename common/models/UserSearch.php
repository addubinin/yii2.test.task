<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.12.2018
 * Time: 23:57
 */

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;

class UserSearch extends User {

    public $fullName;

    public function rules()
    {
        return [
            [['username', 'email', 'id', 'fullName'], 'safe']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return null;
        }

        $count = User::find()->where(['<>', 'username', 'admin'])->count();

        $userQuery = User::find()
            ->where(['<>', 'username', 'admin']);

        $arFilterFields = [
            'id',
            'username',
            'email'
        ];
        foreach ($arFilterFields as $filterField) {
            if (!empty($this->$filterField)) {
                $userQuery->andWhere([$filterField => $this->$filterField]);
            }
        }

        if (!empty($this->fullName)) {
            $userQuery->andWhere(['OR',
                ['LIKE', 'first_name', "{$this->fullName}"],
                ['LIKE', 'second_name', "{$this->fullName}"],
                ['LIKE', 'initial', "{$this->fullName}"]
            ]);
        }

        $userProvider = new ActiveDataProvider([
            'query' => $userQuery,
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'username',
                    'email',
                    'fullName' => [
                        'asc' => ['first_name' => SORT_ASC, 'initial' => SORT_ASC, 'second_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'initial' => SORT_DESC, 'second_name' => SORT_DESC],
                        'label' => 'Full Name',
                        'default' => SORT_ASC
                    ],
                ],
            ],
        ]);

        return $userProvider;
    }

}