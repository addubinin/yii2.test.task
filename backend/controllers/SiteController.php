<?php
namespace backend\controllers;

use common\models\User;
use common\models\UserForm;
use common\models\UserSearch;
use Yii;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['user', 'create-user', 'user-update', 'user-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUser()
    {
        $searchModel = new UserSearch();
        $userProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('user/index', ['userProvider' => $userProvider, 'searchModel' => $searchModel]);
    }

    public function actionCreateUser()
    {
        $userForm = new UserForm();
        $params = Yii::$app->request->post();
        $userForm->load($params);
        if (!empty($params) && $userForm->validate()) {
            $userForm->makeUser();
            return $this->redirect('/site/user');
        }
        return $this->render('user/form', ['userForm' => $userForm, 'action' => 'site/create-user']);
    }

    public function actionUserUpdate($id)
    {
        $userForm = new UserForm();
        $user = User::findOne(['id' => $id]);
        if (!$user) return $this->redirect('/site/user');
        $userForm->setUser($user);

        $params = Yii::$app->request->post();
        if (!empty($params)) {
            $userForm->load($params);
        }

        if (!empty($params) && $userForm->validate()) {
            $userForm->updateUser($id);
            return $this->redirect('/site/user');
        }

        return $this->render('user/form', ['userForm' => $userForm, 'action' => 'site/user-update?id=' . $id]);
    }

    public function actionUserDelete($id)
    {
        User::deleteAll(['id' => $id]);

        return $this->redirect('/site/user');
    }
}
