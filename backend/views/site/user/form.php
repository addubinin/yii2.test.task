<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.12.2018
 * Time: 23:17
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin([
    'id' => 'signup-form',
    'action' => [$action],
    'method' => 'post'
]);
?>


<?= $form->field($userForm, 'username')->textInput(['placeholder' => 'Логин'])->label('Логин') ?>
<?= $form->field($userForm, 'password')->passwordInput(['placeholder' => 'Пароль'])->label('Пароль') ?>
<?= $form->field($userForm, 'email')->textInput(['placeholder' => 'Электронный адрес'])->label('Email') ?>
<?= $form->field($userForm, 'firstName')->textInput(['placeholder' => 'Имя'])->label('Имя') ?>
<?= $form->field($userForm, 'initial')->textInput(['placeholder' => 'Отчество'])->label('Отчество') ?>
<?= $form->field($userForm, 'secondName')->textInput(['placeholder' => 'Фамилия'])->label('Фамилия') ?>
<?= $form->field($userForm, 'subLastDate', [
        'template' => "{label}\n<span class=\"clearable\">{input}<i class=\"clearable__clear\">&times;</i></span>\n{hint}\n{error}",
    ])->widget(\yii\jui\DatePicker::class, [
    'language' => 'ru',
    'options' => [
        'autocomplete' => 'off',
        'readOnly' => true,
    ],
    'dateFormat' => 'dd.MM.yyyy',
    'clientOptions' => [
        'changeMonth' => true,
        'changeYear' => true,
        'yearRange' => "2018:2050",
    ],
])->label('Дата окончания подписки') ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
</div>

<? ActiveForm::end(); ?>
