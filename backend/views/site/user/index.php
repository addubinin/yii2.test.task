<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.12.2018
 * Time: 23:01
 */

use \yii\grid\GridView;
use \yii\helpers\Html;
use \yii\bootstrap\ActiveForm;

?>

<div class="form-group">
    <a href="/site/create-user" class="btn btn-success">Добавить пользователя</a>
</div>

<div class="form-group">
    <?= GridView::widget([
        'dataProvider' => $userProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email',
            'fullName',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{update}{delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'update') {
                        $url = '/site/user-update?id=' . $model['id'];
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = '/site/user-delete?id=' . $model['id'];
                        return $url;
                    }

                }
            ],
        ],
    ]); ?>
</div>