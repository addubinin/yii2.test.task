<?php
/**
 * Created by PhpStorm.
 * User: Dubinin
 * Date: 17.01.2019
 * Time: 11:34
 */

namespace app\models;

use common\models\User;

class EnvAuthenticate implements Auth {

    public function authenticate($login, $password)
    {
        $envLogin = getenv('AUTH_LOGIN');
        $envPass = getenv('AUTH_PASSWORD');

        if ($envLogin !== $login || $envPass !== $password) {
            return null;
        } else {
            $user = new User();
            $user->username = $envLogin;
            $user->setPassword($envPass);
            return $user;
        }
    }

}