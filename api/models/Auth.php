<?php
/**
 * Created by PhpStorm.
 * User: Dubinin
 * Date: 17.01.2019
 * Time: 11:32
 */

namespace app\models;

/**
 * Аутентификация
 * Interface IAuth
 */
interface Auth {
    /**
     * @param $login
     * @param $password
     * @return \common\models\User
     */
    public function authenticate($login, $password);
}
